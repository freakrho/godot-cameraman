#if TOOLS
using Godot;

namespace CameraMan {
    [Tool]
    public class CameraManPlugin : EditorPlugin {
        const string ROOT = "addons/CameraMan";
        const string CONTROLLERS = ROOT + "/Controllers";
        static readonly System.Type[] EFFECTOR_LIST = {
            typeof(VCamTranslator),
            typeof(VCamHardLookAt),
            typeof(VCamFramingTranslator),
            typeof(VCamSmoothAimController),
        };


        VirtualCameraGizmo virtualCameraGizmo;

        public override void _EnterTree() {
            base._EnterTree();
            // Scripts
            var cameraMan = GD.Load<Script>($"{ROOT}/CameraMan.cs");
            var virtualCamera = GD.Load<Script>($"{ROOT}/VirtualCamera.cs");
            // Textures
            var texture = GD.Load<Texture>($"{ROOT}/Icons/virtual-camera.png");
            var managerTexture = GD.Load<Texture>($"{ROOT}/Icons/virtual-camera-manager.png");
            var controllerTexture = GD.Load<Texture>($"{ROOT}/Icons/virtual-camera-controller.png");

            AddCustomType("CameraMan", "Camera", cameraMan, managerTexture);
            AddCustomType("VirtualCamera", "Spatial", virtualCamera, texture);

            foreach (var effector in EFFECTOR_LIST) {
                AddEffectorCustomType(effector, controllerTexture);
            }

            virtualCameraGizmo = new VirtualCameraGizmo();
            AddSpatialGizmoPlugin(virtualCameraGizmo);
        }

        void AddEffectorCustomType(System.Type type, Texture icon) {
            var script = GD.Load<Script>($"{CONTROLLERS}/{type.Name}.cs");
            AddCustomType(type.Name, "Node", script, icon);
        }

        public override void _ExitTree() {
            base._ExitTree();
            RemoveCustomType("CameraMan");
            RemoveCustomType("VirtualCamera");

            foreach (var effector in EFFECTOR_LIST) {
                RemoveCustomType(effector.Name);
            }
            
            RemoveSpatialGizmoPlugin(virtualCameraGizmo);
        }
    }
}
#endif
