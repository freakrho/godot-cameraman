using System.Collections.Generic;
using Godot;

namespace CameraMan {
    [Tool]
    public class CameraMan : Camera {
        struct CameraValues {
            public float HOffset;
            public float VOffset;
            public float Size;
            public float Fov;
            public float Near;
            public float Far;
        }
        [Export] Transition defaultTransition;

        static CameraMan inst;

        List<VirtualCamera> cameras = new List<VirtualCamera>();
        bool dirty;
        VirtualCamera currentCamera;
        // Transition
        bool transitioning;
        Transform startPosition;
        Transition transitionData;
        float transitionDuration;
        float transitionTime;
        CameraValues startValues;
#if TOOLS
        bool loaded;
#endif

        public override void _EnterTree() {
            base._EnterTree();
#if TOOLS
            if (defaultTransition == null) {
                defaultTransition =
                    GD.Load<Transition>("res://addons/CameraMan/Defaults/DefaultTransition.tres")
                        .MakeCopy();
            }
#endif

            inst = this;

            LookForVirtualCameras(GetTree().Root);
#if TOOLS
            loaded = true;
#endif
        }

        public override void _Process(float delta) {
            base._Process(delta);
#if TOOLS
            if (!loaded) {
                _EnterTree();
            }
#endif
            if (cameras.Count == 0) return;
            
            if (dirty) {
                TransitionTo(cameras[0]);
                dirty = false;
            }

            if (transitioning) {
                if (transitionTime < transitionDuration) {
                    float t = transitionTime / transitionDuration;
                    GlobalTransform = startPosition.InterpolateWith(
                        currentCamera.GlobalTransform,
                        transitionData.TimeCurve.InterpolateBaked(t)
                    );
                    HOffset = Mathf.Lerp(startValues.HOffset, currentCamera.HOffset, t);
                    VOffset = Mathf.Lerp(startValues.VOffset, currentCamera.VOffset, t);
                    Size = Mathf.Lerp(startValues.Size, currentCamera.Size, t);
                    Fov = Mathf.Lerp(startValues.Fov, currentCamera.Fov, t);
                    Near = Mathf.Lerp(startValues.Near, currentCamera.Near, t);
                    Far = Mathf.Lerp(startValues.Far, currentCamera.Far, t);
                } else {
                    GlobalTransform = currentCamera.GlobalTransform;
                    transitioning = false;
                }
                transitionTime += delta;
            } else {
                GlobalTransform = currentCamera.GlobalTransform;
                HOffset = currentCamera.HOffset;
                VOffset = currentCamera.VOffset;
                Size = currentCamera.Size;
                Fov = currentCamera.Fov;
                Near = currentCamera.Near;
                Far = currentCamera.Far;
            }
        }

        VirtualCamera GetFirstVisibleVirtualCamera() {
            foreach (var cam in cameras) {
                if (cam.Visible) return cam;
            }
            return null;
        }

        Transition GetTransition(VirtualCamera from, VirtualCamera to) {
            return defaultTransition;
        }

        void TransitionTo(VirtualCamera cam) {
            if (cam == currentCamera) return;
            transitionData = GetTransition(currentCamera, cam);
            currentCamera = cam;
            transitionDuration = (cam.GlobalTransform.origin - GlobalTransform.origin).Length()
                / transitionData.Speed;
            transitionTime = 0;
            transitioning = true;
            startPosition = GlobalTransform;
            startValues = new CameraValues {
                HOffset = HOffset,
                VOffset = VOffset,
                Fov = Fov,
                Size = Size,
                Near = Near,
                Far = Far,
            };
        }

        void LookForVirtualCameras(Node node) {
            for (int i = 0; i < node.GetChildCount(); i++) {
                var child = node.GetChild(i);
                if (child is VirtualCamera) {
                    AddVirtualCamera(child as VirtualCamera);
                }
                LookForVirtualCameras(child);
            }
        }

        public static void AddVirtualCamera(VirtualCamera cam) {
            if (inst == null) {
                inst = Utils.LookForNode<CameraMan>(cam.GetTree().Root);
                if (inst == null) return;
            }

            if (inst.cameras.Contains(cam)) return;
            
            int index = inst.cameras.Count;
            for (int i = 0; i < inst.cameras.Count; i++) {
                if (inst.cameras[i].Priority < cam.Priority) {
                    index = i;
                    break;
                }
            }
            inst.cameras.Insert(index, cam);

            inst.dirty = true;
        }

        public static void RemoveVirtualCamera(VirtualCamera cam) {
            inst.cameras.Remove(cam);
            inst.dirty = true;
        }

        public static void ChangedPriority(VirtualCamera cam) {
            if (cam.IsVisibleInTree()) {
                RemoveVirtualCamera(cam);
                AddVirtualCamera(cam);
            }
        }
    }
}
