using Godot;

namespace CameraMan {
    [Tool]
    public class Transition : Resource {
        [Export] public float Speed = 10f;
        [Export] public Curve TimeCurve;

        public Transition MakeCopy() {
            var result = Duplicate() as Transition;
            result.TimeCurve = TimeCurve.Duplicate() as Curve;
            return result;
        }
    }
}
