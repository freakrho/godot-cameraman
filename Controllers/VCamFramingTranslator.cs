using Godot;

namespace CameraMan {
    [Tool]
    public class VCamFramingTranslator : VCamController {
        [Export] Vector3 offset;
        [Export(PropertyHint.Range, "0.0,1.0")] float deadZoneX;
        [Export(PropertyHint.Range, "0.0,1.0")] float deadZoneY;
        [Export(PropertyHint.Range, "0.0,1.0")] float deadZoneZ;
        [Export(PropertyHint.Range, "0.0,1.0")] float softZoneX = .25f;
        [Export(PropertyHint.Range, "0.0,1.0")] float softZoneY = .25f;
        [Export(PropertyHint.Range, "0.0,1.0")] float softZoneZ = .25f;
        [Export] float lookAheadTime = 0;
        [Export] float smoothTime = 0.1f;
        [Export] float maxSpeed = 10f;

        protected override bool Valid => base.Valid && HasFollowTarget;

        Vector3 velocity;
        Vector3 lastPos;
        float speed;
        float speedVelocity;

        public override void _Ready() {
            base._Ready();
            if (Valid) {
                lastPos = Parent.FollowTarget.GlobalTransform.origin;
            }
        }

        protected override void ProcessEffector(float delta) {
            base.ProcessEffector(delta);
            
            var transform = Parent.GlobalTransform;
            var origin = transform.origin;
            var forward = -transform.basis.z.Normalized();
            var right = transform.basis.x.Normalized();
            var up = transform.basis.y.Normalized();
            var target = Parent.FollowTarget.GlobalTransform.origin;
            var deltaPos = target - lastPos;
            lastPos = target;
            var vel = deltaPos.Length() / delta;
            speed = Utils.SmoothDamp(speed, vel, ref speedVelocity, smoothTime, maxSpeed, delta);
            target += deltaPos.Normalized() * speed * delta;

            target += offset;
            
            // Z position is done simply with distance along z axis and before
            // anything else to ensure target is in front of camera
            var relPos = target - origin;
            var sign = forward.AngleTo(relPos) > Mathf.Pi / 2 ? -1 : 1;
            var zDistance = sign * forward.Dot(relPos);
            var zSize = (Parent.Far - Parent.Near);
            var zCenter = Parent.Near + zSize * .5f;
            
            if (zDistance > zCenter - deadZoneZ * zSize * .5f && zDistance < zCenter + deadZoneZ * zSize * .5f) {
                // Do nothing
            } else if (zDistance > zCenter - softZoneZ * zSize * .5f && zDistance < zCenter + softZoneZ * zSize * .5f) {
                // Ease in
                var diff = zDistance
                    - Mathf.Clamp(
                        zDistance,
                        zCenter - deadZoneZ * zSize * .5f,
                        zCenter + deadZoneZ * zSize * .5f);
                transform.origin = Utils.SmoothDamp(
                    transform.origin,
                    transform.origin + forward * diff,
                    ref velocity,
                    smoothTime, maxSpeed, delta
                );
            } else {
                // Hard move
                var diff = zDistance
                    - Mathf.Clamp(
                        zDistance,
                        zCenter - softZoneZ * zSize * .5f,
                        zCenter + softZoneZ * zSize * .5f);
                transform.origin += forward * diff;
            }

            // This is in case the target is placed behind the camera which will mess with the calculations
            origin = transform.origin;
            forward = -transform.basis.z;
            right = transform.basis.x;
            up = transform.basis.y;

            var hFov = GetHFov();
            var vFov = GetVFov();

            // Planes
            var hPlane = new Plane(
                origin,
                origin + forward,
                origin + right
            );
            var vPlane = new Plane(
                origin,
                origin + forward,
                origin + up
            );
            
            // Relative positions
            var hPos = hPlane.Project(target) - origin;
            var vPos = vPlane.Project(target) - origin;

            // Angles
            var hAngle = forward.AngleTo(hPos);
            var vAngle = forward.AngleTo(vPos);

            var centerPos = forward * hPos.Length() * Mathf.Cos(hAngle);
            
            // Dirs
            var hDir = hPos - centerPos;
            var vDir = vPos - centerPos;

            Move(hAngle, hFov, hDir, deadZoneX, softZoneX, ref transform, delta);
            Move(vAngle, vFov, vDir, deadZoneY, softZoneY, ref transform, delta);

            // Apply new position
            Parent.GlobalTransform = transform;
        }

        void Move(
            float angle, float fov, Vector3 dir,
            float deadZone, float softZone,
            ref Transform transform, float delta
        ) {
            if (angle < fov * deadZone) {
                // Do nothing
            } else if (angle < fov * softZone) {
                // Ease in
                var diffAngle = fov * deadZone - angle;
                dir = angle / (fov * deadZone) * dir;
                transform.origin = Utils.SmoothDamp(
                    transform.origin,
                    transform.origin + dir,
                    ref velocity,
                    smoothTime, maxSpeed, delta
                );
            } else { // Hard move
                // Distance is defined by angle
                var diffAngle = fov * softZone - angle;
                dir = angle / (fov * softZone) * dir;
                transform.origin += dir;
            }
        }

        public override void OnDrawGizmos(
            VirtualCameraGizmo gizmoPlugin,
            EditorSpatialGizmo gizmo,
            float near, float far,
            Vector3 dirUpLeft,
            Vector3 dirUpRight,
            Vector3 dirDownLeft,
            Vector3 dirDownRight
        ) {
            // Near
            var width = (dirUpLeft - dirUpRight).Length();
            var height = (dirUpLeft - dirDownLeft).Length();
            
            var lines = new Vector3[24];
            CreateFrustum(width, height, near, far,
                dirUpLeft, dirUpRight, dirDownLeft, dirDownRight,
                softZoneX, softZoneY, lines);
            gizmo.AddLines(lines, gizmoPlugin.GetMaterial("line-blue"), false);

            CreateFrustum(width, height, near, far,
                dirUpLeft, dirUpRight, dirDownLeft, dirDownRight,
                deadZoneX, deadZoneY, lines);
            gizmo.AddLines(lines, gizmoPlugin.GetMaterial("line-black"), false);
        }

        void CreateFrustum(
            float width, float height,
            float near, float far,
            Vector3 dirUpLeft,
            Vector3 dirUpRight,
            Vector3 dirDownLeft,
            Vector3 dirDownRight,
            float xZone, float yZone,
            Vector3[] lines
        ) {
            var upLeft = dirUpLeft
                + Vector3.Right * width * .5f * (1 - xZone)
                - Vector3.Up * height * .5f * (1 - yZone);
            var downLeft = dirDownLeft
                + Vector3.Right * width * .5f * (1 - xZone)
                + Vector3.Up * height * .5f * (1 - yZone);
            var upRight = dirUpRight
                - Vector3.Right * width * .5f * (1 - xZone)
                - Vector3.Up * height * .5f * (1 - yZone);
            var downRight = dirDownRight
                - Vector3.Right * width * .5f * (1 - xZone)
                + Vector3.Up * height * .5f * (1 - yZone);
            
            
            lines[0] = upLeft * near;
            lines[1] = downLeft * near;
            lines[2] = upRight * near;
            lines[3] = downRight * near;
            lines[4] = upLeft * near;
            lines[5] = upRight * near;
            lines[6] = downLeft * near;
            lines[7] = downRight * near;
            lines[8] = upLeft * far;
            lines[9] = downLeft * far;
            lines[10] = upRight * far;
            lines[11] = downRight * far;
            lines[12] = upLeft * far;
            lines[13] = upRight * far;
            lines[14] = downLeft * far;
            lines[15] = downRight * far;
            lines[16] = upLeft * near;
            lines[17] = upLeft * far;
            lines[18] = downLeft * near;
            lines[19] = downLeft * far;
            lines[20] = upRight * near;
            lines[21] = upRight * far;
            lines[22] = downRight * near;
            lines[23] = downRight * far;
        }
    }
}
