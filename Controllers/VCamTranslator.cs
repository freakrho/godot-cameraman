using Godot;

namespace CameraMan {
    [Tool]
    public class VCamTranslator : VCamController {
        [Export] Vector3 Offset;

        protected override bool Valid => base.Valid && HasFollowTarget;

        protected override void ProcessEffector(float delta) {
            base.ProcessEffector(delta);

            Parent.SetPosition(Parent.FollowTarget.GlobalTransform.origin + Offset);
        }
    }
}
