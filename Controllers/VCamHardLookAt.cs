using Godot;

namespace CameraMan {
    [Tool]
    public class VCamHardLookAt : VCamController {
        protected override bool Valid => base.Valid && HasLookTarget;
        
        protected override void ProcessEffector(float delta) {
            base.ProcessEffector(delta);

            Parent.LookAt(Parent.LookTarget.GlobalTransform.origin, Vector3.Up);
        }
    }
}
