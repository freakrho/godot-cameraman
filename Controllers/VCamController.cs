using Godot;

namespace CameraMan {
    public class VCamController : Node {
        [Export] public bool IsOn = true;
        
        public VirtualCamera Parent;

        protected virtual bool Valid => Parent != null;
        public bool HasFollowTarget => Parent.FollowTarget != null;
        public bool HasLookTarget => Parent.LookTarget != null;

        public override void _EnterTree() {
            FindParent();
        }

        void FindParent() {
            // Get parent virtual camera
            var parent = GetParent();
            if (parent is VirtualCamera) {
                Parent = parent as VirtualCamera;
                GD.Print($"Parent correctly set {parent}");
            }
        }

        public override void _Process(float delta) {
            base._Process(delta);
            if (!IsOn) return;

            if (Parent == null) {
                FindParent();
                if (Parent == null) {
                    GD.PushError($"{this} No virtual camera");
                }
            }

            if (!Valid) return;
            ProcessEffector(delta);
        }

        protected virtual void ProcessEffector(float delta) {

        }
        
        // TODO: Make this depending on Keep Aspect value
        /// <summary>
        /// Half vertical FOV in radians
        /// </summary>
        protected float GetVFov() {
            return Mathf.Deg2Rad(Parent.Fov * .5f);
        }
        /// <summary>
        /// Half horizontal FOV in radians
        /// </summary>
        protected float GetHFov() {
            return Mathf.Deg2Rad(
                Parent.Fov * OS.WindowSize.x / OS.WindowSize.y) * .5f;
        }


        // Gizmos
        public virtual void OnDrawGizmos(
            VirtualCameraGizmo gizmoPlugin,
            EditorSpatialGizmo gizmo,
            float near, float far,
            Vector3 dirUpLeft,
            Vector3 dirUpRight,
            Vector3 dirDownLeft,
            Vector3 dirDownRight
        ) {

        }
    }
}
