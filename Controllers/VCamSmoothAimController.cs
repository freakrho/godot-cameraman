using Godot;

namespace CameraMan {
    [Tool]
    public class VCamSmoothAimController : VCamController {
        const float MIN_DIFF = 0.01f;

        [Export] Vector3 offset;
        [Export(PropertyHint.Range, "0.0,1.0")] float deadZone = .1f;
        [Export(PropertyHint.Range, "0.0,1.0")] float softZone = .25f;
        [Export] float lookAheadTime = 0;
        [Export] float smoothTime = 0.1f;
        [Export] float maxSpeed = 10f;

        Vector3 velocity;
        Vector3 lastPos;
        float speed;
        float speedVelocity;

        protected override bool Valid => base.Valid && HasLookTarget;

        public override void _Ready() {
            base._Ready();
            if (Valid) {
                lastPos = Parent.LookTarget.GlobalTransform.origin;
            }
        }

        protected override void ProcessEffector(float delta) {
            base.ProcessEffector(delta);

            if (softZone < deadZone + MIN_DIFF) {
                softZone = deadZone + MIN_DIFF;
            }

            var hFov = GetHFov();
            var vFov = GetVFov();
            var transform = Parent.GlobalTransform;
            var forward = -transform.basis.z;
            var target = Parent.LookTarget.GlobalTransform.origin;
            var deltaPos = target - lastPos;
            lastPos = target;
            var vel = deltaPos.Length() / delta;
            speed = Utils.SmoothDamp(speed, vel, ref speedVelocity, smoothTime, maxSpeed, delta);
            target += deltaPos.Normalized() * speed * delta;
            
            target += offset;

            var fov = (hFov + vFov) / 2;

            var angle = forward.AngleTo(target - transform.origin);
            
            var mul = 1f;
            if (angle < fov * deadZone) {
                // Do nothing
                return;
            } else if (angle > fov * softZone) {
                mul = 10f;
            }

            var targetRotation = transform.LookingAt(target, Vector3.Up).basis.GetEuler();
            // Ease in
            var rotation = Utils.SmoothDampEuler(
                transform.basis.GetEuler(), targetRotation,
                ref velocity, smoothTime, maxSpeed, delta * mul
            );
            transform = new Transform(new Basis(rotation), transform.origin);
            Parent.GlobalTransform = transform;
        }
    }
}
