using Godot;

namespace CameraMan {
    public class VirtualCameraGizmo : EditorSpatialGizmoPlugin {
        public VirtualCameraGizmo() {
            CreateMaterial("line", Colors.White);
            CreateMaterial("line-red", Colors.Red);
            CreateMaterial("line-blue", Colors.Red);
            CreateMaterial("line-black", Colors.Black);
        }
        
        public override string GetName() {
            return "VirtualCamera";
        }

        public override bool HasGizmo(Spatial spatial) {
            return spatial is VirtualCamera;
        }

        public override void Redraw(EditorSpatialGizmo gizmo) {
            gizmo.Clear();

            var cam = gizmo.GetSpatialNode() as VirtualCamera;

            // Assume 16:9
            var hAngle = Mathf.Deg2Rad(cam.Fov * OS.WindowSize.x / OS.WindowSize.y) * .5f;

            var dirUp = Vector3.Forward.Rotated(
                Vector3.Right, Mathf.Deg2Rad(cam.Fov * .5f));
            var dirDown = Vector3.Forward.Rotated(
                Vector3.Right, Mathf.Deg2Rad(-cam.Fov * .5f));

            var dirUpLeft = dirUp.Rotated(Vector3.Up, hAngle);
            var dirUpRight = dirUp.Rotated(Vector3.Up, -hAngle);
            var dirDownLeft = dirDown.Rotated(Vector3.Up, hAngle);
            var dirDownRight = dirDown.Rotated(Vector3.Up, -hAngle);

            var near = cam.Near / Mathf.Cos(Mathf.Deg2Rad(cam.Fov * .5f))
                / Mathf.Cos(hAngle);
            var far = cam.Far / Mathf.Cos(Mathf.Deg2Rad(cam.Fov * .5f))
                / Mathf.Cos(hAngle);

            var lines = new Vector3[] {
                // Square near
                dirUpLeft * near, dirUpRight * near,
                dirDownLeft * near, dirDownRight * near,
                dirDownLeft * near, dirUpLeft * near,
                dirDownRight * near, dirUpRight * near,
                // Direction
                dirUpLeft * near, dirUpLeft * far,
                dirDownLeft * near, dirDownLeft * far,
                dirUpRight * near, dirUpRight * far,
                dirDownRight * near, dirDownRight * far,
                // Square far
                dirUpLeft * far, dirUpRight * far,
                dirDownLeft * far, dirDownRight * far,
                dirDownLeft * far, dirUpLeft * far,
                dirDownRight * far, dirUpRight * far,
            };

            gizmo.AddLines(lines, GetMaterial("line"), false);

            // Draw effector gizmos
            for (int i = 0; i < cam.GetChildCount(); i++) {
                var child = cam.GetChild(i);
                if (child is VCamController) {
                    (child as VCamController).OnDrawGizmos(
                        this, gizmo, near, far,
                        dirUpLeft, dirUpRight,
                        dirDownLeft, dirDownRight
                    );
                }
            }
        }
    }
}
