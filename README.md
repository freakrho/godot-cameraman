# Godot CameraMan

**Godot CameraMan** is an addon for controlling [godot](https://godotengine.org/)'s 3D camara in interesting ways.

This tool was made for [The Tool Jam](https://itch.io/jam/the-tool-jam-2), you can get it in [itch.io](https://freakrho.itch.io/godot-cameraman)

![](Help/example.gif)

## Installation

Copy this repository's contents to addons/CameraMan in your project and then enable it in **Project/Project Settings.../Plugins**

You can add this as a submodule in your git repository, just cd to your project's root and run:

    git submodule add git@gitlab.com:freakrho/godot-cameraman.git addons/CameraMan

Make sure your project looks like this (the addon's root directory name is important)

![](Help/instructions-00.png)

Then commit your changes and you're done.

## Usage

With this addon we use one CameraMan node and control it with **Virtual Cameras**

![](Help/instructions-01.png) ![](Help/instructions-02.png)

The **CameraMan** will always follow the visible **VirtualCamera** with the highest priority, you can move VirtualCameras around yourself or you can use the **VCamControllers**.

![](Help/instructions-03.png)

The controllers will handle following and looking at a target.

There's more information in the [wiki](https://gitlab.com/freakrho/godot-cameraman/-/wikis/home)

## Contributing
[Here is a contribution guide](https://gitlab.com/freakrho/godot-cameraman/-/blob/main/CONTRIBUTING.md) if you want to add to the project.

## License
This project is licensed under an [Apache License 2.0](https://gitlab.com/freakrho/godot-cameraman/-/blob/main/LICENSE)
