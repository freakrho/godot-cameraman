using Godot;

namespace CameraMan {
    [Tool]
    public class VirtualCamera : Spatial {
        [Export] NodePath followTargetNode;
        [Export] NodePath lookTargetNode;
        [Export] public int Priority = 1;
        [Export] public float HOffset = 0f;
        [Export] public float VOffset = 0f;
        [Export] public float Size = 70f;
        [Export(PropertyHint.Range, "1.0, 179.0")] public float Fov = 70f;
        [Export(PropertyHint.Range, "0.01,8192")] public float Near = .05f;
        [Export(PropertyHint.Range, "0.01,8192")] public float Far = 100f;

        public Spatial FollowTarget;
        public Spatial LookTarget;

        int cachedPririty;
#if TOOLS
        bool loaded;
#endif

        void LoadNodes() {
            if (followTargetNode != null && !followTargetNode.IsEmpty()) {
                FollowTarget = GetNode<Spatial>(followTargetNode);
            }
            if (lookTargetNode != null && !lookTargetNode.IsEmpty()) {
                LookTarget = GetNode<Spatial>(lookTargetNode);
            }
        }

        public override void _EnterTree() {
            base._EnterTree();
            LoadNodes();

            cachedPririty = Priority;
            Connect("visibility_changed", this, "OnVisibilityChanged");
#if TOOLS
            loaded = true;
#endif
        }

        public override void _Ready() {
            base._Ready();
            
            CameraMan.AddVirtualCamera(this);
        }

        public override void _ExitTree() {
            base._ExitTree();
            CameraMan.RemoveVirtualCamera(this);
        }

        public override void _Process(float delta) {
            base._Process(delta);
#if TOOLS
            if (!loaded) {
                _EnterTree();
            }

            if (
                FollowTarget == null || LookTarget == null
                ||
                FollowTarget.GetPath() != followTargetNode
                ||
                LookTarget.GetPath() != lookTargetNode
            ) {
                LoadNodes();
            }
#endif
            if (cachedPririty != Priority) {
                cachedPririty = Priority;
                CameraMan.ChangedPriority(this);
            }
        }

        void OnVisibilityChanged() {
            if (IsVisibleInTree()) {
                CameraMan.AddVirtualCamera(this);
            } else {
                CameraMan.RemoveVirtualCamera(this);
            }
        }

        /// Set Global Position
        public void SetPosition(Vector3 position) {
            var transform = GlobalTransform;
            transform.origin = position;
            GlobalTransform = transform;
        }
    }
}
