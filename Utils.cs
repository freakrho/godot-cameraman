using Godot;

namespace CameraMan {
    public static class Utils {
        public static T LookForNode<T>(Node node) where T : Node {
            for (int i = 0; i < node.GetChildCount(); i++) {
                var child = node.GetChild(i);
                if (child is T) {
                    return child as T;
                }
                var newNode = LookForNode<T>(child);
                if (newNode != null) return newNode;
            }
            return null;
        }

        public static float SmoothDamp(
            float current,
            float target,
            ref float currentVelocity,
            float smoothTime,
            float maxSpeed,
            float deltaTime
        ) {
            // Based on Game Programming Gems 4 Chapter 1.10
            smoothTime = Mathf.Max(0.0001F, smoothTime);
            float omega = 2F / smoothTime;

            float x = omega * deltaTime;
            float exp = 1F / (1F + x + 0.48F * x * x + 0.235F * x * x * x);
            float change = current - target;
            float originalTo = target;

            // Clamp maximum speed
            float maxChange = maxSpeed * smoothTime;
            change = Mathf.Clamp(change, -maxChange, maxChange);
            target = current - change;

            float temp = (currentVelocity + omega * change) * deltaTime;
            currentVelocity = (currentVelocity - omega * temp) * exp;
            float output = target + (change + temp) * exp;

            // Prevent overshooting
            if (originalTo - current > 0.0F == output > originalTo) {
                output = originalTo;
                currentVelocity = (output - originalTo) / deltaTime;
            }

            return output;
        }

        public static Vector3 SmoothDamp(
            Vector3 current,
            Vector3 target,
            ref Vector3 velocity,
            float smoothTime,
            float maxSpeed,
            float deltaTime
        ) {
            return new Vector3(
                SmoothDamp(current.x, target.x, ref velocity.x, smoothTime, maxSpeed, deltaTime),
                SmoothDamp(current.y, target.y, ref velocity.y, smoothTime, maxSpeed, deltaTime),
                SmoothDamp(current.z, target.z, ref velocity.z, smoothTime, maxSpeed, deltaTime)
            );
        }

        public static Vector3 SmoothDampEuler(
            Vector3 current,
            Vector3 target,
            ref Vector3 velocity,
            float smoothTime,
            float maxSpeed,
            float deltaTime
        ) {
            return new Vector3(
                SmoothDampRad(current.x, target.x, ref velocity.x, smoothTime, maxSpeed, deltaTime),
                SmoothDampRad(current.y, target.y, ref velocity.y, smoothTime, maxSpeed, deltaTime),
                SmoothDampRad(current.z, target.z, ref velocity.z, smoothTime, maxSpeed, deltaTime)
            );
        }

        /// <summary>
        /// Signed difference between angles in radians
        /// </summary>
        public static float AngleDiffRad(float firstAngle, float secondAngle) {
            float difference = secondAngle - firstAngle;
            while (difference < -Mathf.Pi) difference += Mathf.Tau;
            while (difference > Mathf.Pi) difference -= Mathf.Tau;
            return difference;
        }

        public static float SmoothDampRad(
            float current, float target,
            ref float velocity,
            float smoothTime, float maxSpeed, float deltaTime
        ) {
            target = current + AngleDiffRad(current, target);
            return SmoothDamp(current, target, ref velocity, smoothTime,
                maxSpeed, deltaTime);
        }
    }
}
